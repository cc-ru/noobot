table! {
    messages (id) {
        id -> Int4,
        channel -> Text,
        author -> Text,
        body -> Text,
        sent_at -> Timestamp,
        line_number -> Int4,
        date_sent -> Date,
    }
}

table! {
    user_timezones (nickname) {
        nickname -> Text,
        timezone -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    messages,
    user_timezones,
);
