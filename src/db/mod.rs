pub mod models;
pub mod schema;

use std::marker::PhantomData;

use diesel::r2d2::{ConnectionManager, Pool, PooledConnection};
use diesel::{Connection, PgConnection};

use actix::prelude::*;

use failure::{Error, Fallible};
use futures::Future;

use crate::config::Config;

#[derive(Clone)]
pub struct DbExecutor {
    pub pool: Pool<ConnectionManager<PgConnection>>,
    pub config: Config,
}

impl DbExecutor {
    pub fn new(c: &Config) -> Fallible<DbExecutor> {
        let connection = ConnectionManager::new(c.database.url.clone());
        let pool = Pool::builder()
            .max_size(c.database.max_size)
            .min_idle(c.database.min_idle)
            .max_lifetime(c.database.max_lifetime)
            .idle_timeout(c.database.idle_timeout)
            .connection_timeout(c.database.connection_timeout)
            .build(connection)?;

        Ok(DbExecutor {
            pool,
            config: c.clone(),
        })
    }
}

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

pub struct DbTask<F, C, T>
where
    F: FnOnce(C) -> Fallible<T>,
    C: Connection,
{
    f: F,
    _marker: std::marker::PhantomData<(C, T)>,
}

impl<F, C, T: 'static> Message for DbTask<F, C, T>
where
    F: FnOnce(C) -> Fallible<T>,
    C: Connection,
{
    type Result = Fallible<T>;
}

type Conn = PooledConnection<ConnectionManager<PgConnection>>;

impl<F, T: 'static> Handler<DbTask<F, Conn, T>> for DbExecutor
where
    F: FnOnce(Conn) -> Fallible<T>,
{
    type Result = Fallible<T>;

    fn handle(&mut self, msg: DbTask<F, Conn, T>, _: &mut Self::Context) -> Self::Result {
        let conn = self.pool.get()?;
        (msg.f)(conn)
    }
}

pub fn execute<F, T>(db: &Addr<DbExecutor>, f: F) -> impl Future<Item = T, Error = Error>
where
    F: FnOnce(Conn) -> Fallible<T> + Send + 'static,
    T: Send + 'static,
{
    db.send(DbTask {
        f,
        _marker: PhantomData,
    }).from_err()
        .and_then(|v| v.map_err(From::from))
}
