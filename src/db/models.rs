use chrono::{NaiveDate, NaiveDateTime};

use super::schema::*;

#[derive(Insertable)]
#[table_name = "messages"]
pub struct NewMessage<'a> {
    pub channel: &'a str,
    pub author: &'a str,
    pub body: &'a str,
    pub line_number: i32,
}

#[derive(Insertable)]
#[table_name = "messages"]
pub struct NewMessageAt<'a> {
    pub channel: &'a str,
    pub author: &'a str,
    pub body: &'a str,
    pub line_number: i32,
    pub sent_at: NaiveDateTime,
    pub date_sent: NaiveDate,
}

#[derive(Queryable)]
pub struct Message {
    pub id: i32,
    pub channel: String,
    pub author: String,
    pub body: String,
    pub sent_at: NaiveDateTime,
    pub line_number: i32,
    pub date_sent: NaiveDate,
}

#[derive(Queryable)]
pub struct UserTimezone {
    pub nickname: String,
    pub timezone: String,
}

#[derive(Insertable, AsChangeset)]
#[table_name = "user_timezones"]
pub struct NewUserTimezone<'a> {
    pub nickname: &'a str,
    pub timezone: &'a str,
}
