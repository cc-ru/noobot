use failure::Error;
use futures::Future;
use reqwest::r#async::Client;

#[derive(Serialize)]
struct Request<'a> {
    content: &'a str,
}

#[derive(Deserialize)]
struct Response {
    url: String,
}

pub fn shorten(url: &str) -> impl Future<Item = String, Error = Error> {
    let req = Request { content: url };

    Client::new()
        .post("https://ptpb.pw/u")
        .header("Accept", "application/json")
        .json(&req)
        .send()
        .and_then(|mut r| r.json::<Response>())
        .from_err()
        .map(|res| res.url)
}

#[cfg(test)]
mod tests {
    use super::*;
    use futures::future;

    #[test]
    fn simple() {
        let f = super::shorten("https://google.com/");
        tokio::run(f.then(|v| {
            assert_eq!(v.unwrap(), "https://ptpb.pw/s_M-".to_string());
            Ok(())
        }));
    }

    #[test]
    fn flood() {
        let futures = (0..10).map(|d| {
            super::shorten(&format!("https://logs.fomalhaut.me/2016-12-{:02}.log", d)).map(|_| ())
        });

        let f = future::join_all(futures);

        tokio::run(f.then(|v| {
            v.unwrap();
            Ok(())
        }));
    }
}
