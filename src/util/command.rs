use std::marker::PhantomData;

use ::irc::proto::{Command, Prefix};
use actix::prelude::*;
use structopt::clap::{AppSettings, ErrorKind};
use structopt::StructOpt;

use crate::config::Config;
use crate::irc::{Irc, IrcMessage};

pub struct CommandParser<T: StructOpt> {
    prefix: char,
    _marker: PhantomData<T>,
}

pub struct ParsedCommand<T> {
    pub channel: String,
    pub author: String,
    pub opts: T,
}

impl<T: StructOpt> CommandParser<T> {
    pub fn new(c: &Config) -> CommandParser<T> {
        CommandParser {
            prefix: c.bot.prefix,
            _marker: Default::default(),
        }
    }

    pub fn parse(&self, irc: &Addr<Irc>, msg: IrcMessage) -> Option<ParsedCommand<T>> {
        let IrcMessage { prefix, command } = msg;

        let author = match prefix {
            Some(Prefix::Nickname(n, _, _)) => n,
            _ => return None,
        };

        let (channel, mut body) = match command {
            Command::PRIVMSG(target, body) => (target, body),
            _ => return None,
        };

        if !channel.starts_with("#") || !body.starts_with(self.prefix) {
            return None;
        }

        body.remove(0);

        if !body.starts_with(T::clap().get_name()) {
            return None;
        }

        let parts = body.split_whitespace();

        let err = match T::from_iter_safe(parts) {
            Ok(opts) => {
                return Some(ParsedCommand {
                    channel,
                    author,
                    opts,
                });
            }
            Err(e) => e,
        };

        if err.kind == ErrorKind::HelpDisplayed {
            irc.do_send(IrcMessage::privmsg(
                channel,
                format!("{}: I'm sending you a pm with help", author),
            ));

            let mut help = vec![];
            let app = T::clap()
                .template("{about}\n{usage}\n{unified}")
                .setting(AppSettings::NoBinaryName)
                .setting(AppSettings::UnifiedHelpMessage)
                .setting(AppSettings::DisableVersion);

            app.write_help(&mut help).unwrap();

            for line in String::from_utf8(help).unwrap().lines() {
                irc.do_send(IrcMessage::privmsg(author.clone(), line.into()));
            }
        } else {
            irc.do_send(IrcMessage::privmsg(
                channel,
                format!("{}: Invalid syntax (try --help)", author),
            ));
        }

        None
    }
}
