#[macro_use]
extern crate structopt;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate lazy_static;

mod config;
mod db;
mod irc;
mod util;

use actix::prelude::*;
use failure::{Fallible, ResultExt};

use crate::irc::{Irc, IrcPlugin, IrcPluginOpts, Subscribe};

fn main_inner() -> Fallible<()> {
    env_logger::init();

    let sys = actix::System::new("noobot");

    let config = config::load().context("failed to load config")?;
    let db = db::DbExecutor::new(&config).context("failed to establish db connection")?;
    let db = SyncArbiter::start(3, move || db.clone());

    let c = config.clone();
    let irc = Supervisor::start(move |_| Irc::new(&c));

    let opts = IrcPluginOpts {
        config: &config,
        pool: db,
        irc: irc.clone(),
    };

    for plugin in inventory::iter::<IrcPlugin> {
        let recipient = (plugin.0)(opts.clone());
        irc.do_send(Subscribe(recipient));
    }

    sys.run();

    Ok(())
}

fn main() {
    let code = match main_inner() {
        Err(e) => {
            eprintln!("error: {}", e);

            for cause in e.iter_causes() {
                eprintln!("     | {}", cause);
            }

            if let Some(backtrace) = e.as_fail().backtrace() {
                eprintln!("{}", backtrace);
            }

            1
        }

        _ => 0,
    };

    std::process::exit(code);
}
