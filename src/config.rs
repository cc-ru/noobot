use std::time::Duration;

use failure::Fallible;
use irc::client::data::Config as IrcConfig;
use serde::{Deserialize, Deserializer};

fn de_duration<'de, D>(deserializer: D) -> Result<Duration, D::Error>
where
    D: Deserializer<'de>,
{
    let s = <&str>::deserialize(deserializer)?;
    parse_duration::parse(s).map_err(serde::de::Error::custom)
}

fn de_option_duration<'de, D>(deserializer: D) -> Result<Option<Duration>, D::Error>
where
    D: Deserializer<'de>,
{
    de_duration(deserializer).map(Some)
}

#[derive(Debug, Clone, Deserialize)]
pub struct DbConfig {
    pub url: String,
    pub max_size: u32,
    pub min_idle: Option<u32>,

    #[serde(default, deserialize_with = "de_option_duration")]
    pub max_lifetime: Option<Duration>,

    #[serde(default, deserialize_with = "de_option_duration")]
    pub idle_timeout: Option<Duration>,

    #[serde(deserialize_with = "de_duration")]
    pub connection_timeout: Duration,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ModuleConfig {
    pub search: crate::irc::logs::search::ModuleSearchConfig,
}

#[derive(Debug, Clone, Deserialize)]
pub struct BotConfig {
    pub prefix: char,
    pub modules: ModuleConfig,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Config {
    pub irc: IrcConfig,
    pub database: DbConfig,
    pub bot: BotConfig,
}

pub fn load() -> Fallible<Config> {
    let contents = std::fs::read_to_string("config.toml")?;
    let config = toml::from_str(&contents)?;

    Ok(config)
}
