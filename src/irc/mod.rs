pub mod logs;
pub mod time;

use std::collections::HashSet;
use std::time::Duration;

use actix::prelude::*;
use actix_derive::Message;

use ::irc::client::ext::ClientExt;
use ::irc::client::{Client, IrcClient, PackedIrcClient};
use ::irc::error::IrcError;
use ::irc::proto::{Command, Message as RawMessage, Prefix};

use futures::stream::Stream;

use crate::config::Config;
use crate::db::DbExecutor;

#[derive(Clone)]
pub struct IrcPluginOpts<'a> {
    pub config: &'a Config,
    pub pool: Addr<DbExecutor>,
    pub irc: Addr<Irc>,
}

pub struct IrcPlugin(pub Box<dyn Fn(IrcPluginOpts<'_>) -> Recipient<IrcMessage>>);

inventory::collect!(IrcPlugin);

impl IrcPlugin {
    pub fn new<F>(f: F) -> IrcPlugin
    where
        F: Fn(IrcPluginOpts<'_>) -> Recipient<IrcMessage> + 'static,
    {
        IrcPlugin(Box::new(f))
    }
}

#[derive(Clone, Debug, Message, PartialEq)]
pub struct IrcMessage {
    pub prefix: Option<Prefix>,
    pub command: Command,
}

impl IrcMessage {
    pub fn privmsg(target: String, text: String) -> IrcMessage {
        IrcMessage {
            prefix: None,
            command: Command::PRIVMSG(target, text),
        }
    }
}

impl From<RawMessage> for IrcMessage {
    fn from(raw: RawMessage) -> IrcMessage {
        IrcMessage {
            prefix: raw.prefix,
            command: raw.command,
        }
    }
}

impl Into<RawMessage> for IrcMessage {
    fn into(self) -> RawMessage {
        RawMessage {
            tags: Default::default(),
            prefix: self.prefix,
            command: self.command,
        }
    }
}

pub struct Irc {
    config: Config,
    subscribers: HashSet<Recipient<IrcMessage>>,
    client: Option<IrcClient>,
}

impl Irc {
    pub fn new(config: &Config) -> Irc {
        Irc {
            config: config.clone(),
            subscribers: Default::default(),
            client: None,
        }
    }
}

impl Actor for Irc {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let timeout = Duration::from_secs(20);

        log::info!("Starting IRC actor");

        let fut = match IrcClient::new_future(self.config.irc.clone()) {
            Ok(v) => v,
            Err(err) => {
                log::error!("Unable to establish IRC connection: {}", err);
                log::error!("Retrying in {:?}", timeout);
                ctx.run_later(timeout, |_, ctx| ctx.stop());
                return;
            }
        };

        let fut = fut
            .into_actor(self)
            .map_err(move |err, _actor, ctx| {
                log::error!("Unable to establish IRC connection: {}", err);
                log::error!("Retrying in {:?}", timeout);
                ctx.run_later(timeout, |_, ctx| ctx.stop());
            })
            .map(|PackedIrcClient(client, fut), actor, ctx| {
                log::info!("IRC connection established");

                let fut = fut.into_actor(actor).map_err(|err, _actor, ctx| {
                    log::error!("IRC error: {}", err);
                    ctx.stop();
                });

                ctx.spawn(fut);
                client.identify().unwrap();

                let stream = client.stream().map(|v| IrcMessage::from(v));
                Irc::add_stream(stream, ctx);

                actor.client = Some(client);
            });

        ctx.spawn(fut);
    }
}

impl StreamHandler<IrcMessage, IrcError> for Irc {
    fn handle(&mut self, msg: IrcMessage, _ctx: &mut Self::Context) {
        for sub in self.subscribers.iter() {
            sub.do_send(msg.clone()).unwrap();
        }
    }
}

impl Supervised for Irc {
    fn restarting(&mut self, _ctx: &mut Self::Context) {
        self.client = None;
    }
}

impl Handler<IrcMessage> for Irc {
    type Result = ();

    fn handle(&mut self, msg: IrcMessage, ctx: &mut Self::Context) {
        let client = match &self.client {
            Some(v) => v,
            None => return,
        };

        if let Err(e) = client.send(msg) {
            log::error!("IRC error: {}", e);
            ctx.stop();
            return;
        }
    }
}

#[derive(Clone, Message)]
pub struct Subscribe(pub Recipient<IrcMessage>);

impl Handler<Subscribe> for Irc {
    type Result = ();

    fn handle(&mut self, msg: Subscribe, _ctx: &mut Self::Context) {
        self.subscribers.insert(msg.0);
    }
}

#[derive(Clone, Message)]
pub struct Unsubscribe(pub Recipient<IrcMessage>);

impl Handler<Unsubscribe> for Irc {
    type Result = ();

    fn handle(&mut self, msg: Unsubscribe, _ctx: &mut Self::Context) {
        self.subscribers.remove(&msg.0);
    }
}
