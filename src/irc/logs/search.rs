use actix::prelude::*;
use chrono::NaiveDateTime;
use diesel::dsl::sql;
use diesel::pg::Pg;
use diesel::prelude::*;
use diesel::sql_types::{BigInt, Float, Int4, Text, Timestamp};

use crate::db::{self, DbExecutor};
use crate::db::models::Message;
use crate::db::schema::messages;
use crate::irc::{Irc, IrcMessage, IrcPlugin};
use crate::util::CommandParser;

#[derive(Debug, Clone, Deserialize)]
pub struct ModuleSearchConfig {
    pub ignore_bots: Vec<String>,
}

#[derive(Debug, StructOpt)]
#[structopt(name = "search", about = "Search messages in logs")]
struct SearchOpts {
    /// Author of the message
    #[structopt(long, short)]
    author: Option<String>,

    /// Channel to search in
    #[structopt(long, short)]
    channel: Option<String>,

    /// Use regex instead of fulltext search
    #[structopt(long, short)]
    regex: bool,

    /// Reverse sorting order
    #[structopt(long, short = "R")]
    reverse: bool,

    /// Sort by time, newest messages first
    #[structopt(short)]
    time: bool,

    /// Search query
    query: Vec<String>,
}

pub struct Search {
    db: Addr<DbExecutor>,
    irc: Addr<Irc>,
    parser: CommandParser<SearchOpts>,
    cfg: ModuleSearchConfig,
}

impl Actor for Search {
    type Context = Context<Self>;
}

impl Handler<IrcMessage> for Search {
    type Result = ();

    fn handle(&mut self, msg: IrcMessage, ctx: &mut Self::Context) {
        let cmd = match self.parser.parse(&self.irc, msg) {
            Some(v) => v,
            None => return,
        };

        let opts = cmd.opts;
        let ignore_bots = self.cfg.ignore_bots.clone();

        let fut = db::execute(&self.db, move |conn| {
            let query = build_query(opts, ignore_bots).limit(3);
            let values = query.load::<(String, String, NaiveDateTime, String, i32)>(&conn)?;

            Ok(values)
        });

        let channel = cmd.channel;
        let channel2 = channel.clone();

        ctx.wait(
            fut.into_actor(self)
                .map_err(move |e, actor, _| {
                    log::error!("Failed to perform search: {}", e);
                    actor.irc.do_send(IrcMessage::privmsg(
                        channel2,
                        format!("an error occurred: {}", e),
                    ));
                })
                .map(move |entries, _, _| {
                    entries.into_iter().map(|entry| {
                        Message {
                            id: -1,
                            channel: entry.0,
                            author: entry.1,
                            sent_at: entry.2,
                            date_sent: entry.2.date(),
                            body: entry.3,
                            line_number: entry.4,
                        }
                    }).collect()
                })
                .map(move |entries, actor, _| {
                    for line in crate::irc::logs::viewer::format_log(entries) {
                        actor
                            .irc
                            .do_send(IrcMessage::privmsg(channel.clone(), line));
                    }
                }),
        );
    }
}

inventory::submit! {
    IrcPlugin::new(|c| {
        Search {
            db: c.pool.clone(),
            irc: c.irc.clone(),
            parser: CommandParser::new(&c.config),
            cfg: c.config.bot.modules.search.clone(),
        }.start().recipient()
    })
}

#[derive(Debug, StructOpt)]
#[structopt(name = "topx", about = "Top 5 authors matching the query")]
struct TopOpts {
    /// Author of the message
    #[structopt(long, short)]
    author: Option<String>,

    /// Channel to search in
    #[structopt(long, short)]
    channel: Option<String>,

    /// Use regex instead of fulltext search
    #[structopt(long, short)]
    regex: bool,

    /// Search query
    query: Vec<String>,
}

pub struct Top {
    db: Addr<DbExecutor>,
    irc: Addr<Irc>,
    parser: CommandParser<TopOpts>,
    cfg: ModuleSearchConfig,
}

impl Actor for Top {
    type Context = Context<Self>;
}

impl Handler<IrcMessage> for Top {
    type Result = ();

    fn handle(&mut self, msg: IrcMessage, ctx: &mut Self::Context) {
        let cmd = match self.parser.parse(&self.irc, msg) {
            Some(v) => v,
            None => return,
        };

        let opts = cmd.opts;
        let ignore_bots = self.cfg.ignore_bots.clone();

        let fut = db::execute(&self.db, move |conn| {
            use self::messages::dsl::*;
            use diesel::dsl::{count, sql};

            let sopts = SearchOpts {
                author: opts.author,
                channel: opts.channel,
                regex: opts.regex,
                query: opts.query,
                time: false,
                reverse: false,
            };

            let query = build_query(sopts, ignore_bots)
                .select((author, sql::<BigInt>("count(body)")))
                .group_by(author)
                .order(count(body).desc())
                .limit(5);

            let values = query.load::<(String, i64)>(&conn)?;

            Ok(values)
        });

        let channel = cmd.channel;
        let channel2 = channel.clone();

        ctx.wait(
            fut.into_actor(self)
                .map_err(move |e, actor, _| {
                    log::error!("Failed to perform search: {}", e);
                    actor.irc.do_send(IrcMessage::privmsg(
                        channel2,
                        format!("an error occurred: {}", e),
                    ));
                })
                .map(move |top, actor, _| {
                    for line in format_top(top) {
                        actor
                            .irc
                            .do_send(IrcMessage::privmsg(channel.clone(), line));
                    }
                }),
        );
    }
}

inventory::submit! {
    IrcPlugin::new(|c| {
        Top {
            db: c.pool.clone(),
            irc: c.irc.clone(),
            parser: CommandParser::new(&c.config),
            cfg: c.config.bot.modules.search.clone(),
        }.start().recipient()
    })
}

type Query<'a> = diesel::query_builder::BoxedSelectStatement<
    'a,
    (Text, Text, Timestamp, Text, Int4),
    messages::table,
    Pg,
>;

fn build_query<'a>(opts: SearchOpts, ignore_bots: Vec<String>) -> Query<'a> {
    use diesel::dsl::not;
    use diesel::pg::expression::dsl::any;

    use self::messages::dsl::*;

    let q = opts.query.join(" ");

    let mut query = if opts.regex {
        messages
            .select((channel, author, sent_at, body, line_number))
            .into_boxed()
    } else {
        messages
            .select((
                channel,
                author,
                sent_at,
                sql::<Text>(
                    "ts_headline('russian', messages.body, plainto_tsquery('russian', ",
                )
                .bind::<Text, _>(q.clone())
                .sql(
                    "), 'HighlightAll = true, StartSel = \"\x02\", StopSel = \"\x0F\"') AS body",
                ),
                line_number,
            ))
            .into_boxed()
    };

    if opts.regex {
        query = query.filter(sql("body ~ ").bind::<Text, _>(q.clone()))
    } else {
        query = query.filter(
            sql("to_tsvector('russian', body) @@ plainto_tsquery('russian', ")
                .bind::<Text, _>(q.clone())
                .sql(")"),
        );
    }

    if opts.time || opts.regex {
        if opts.reverse {
            query = query.order(sent_at.asc());
        } else {
            query = query.order(sent_at.desc());
        }
    } else {
        let a = sql::<Float>("ts_rank(to_tsvector('russian', body), plainto_tsquery('russian',")
            .bind::<Text, _>(q.clone())
            .sql("))");

        if opts.reverse {
            query = query.order(a.asc());
        } else {
            query = query.order(a.desc());
        }
    }

    if let Some(a) = opts.author {
        query = query.filter(author.eq(a));
    } else {
        query = query.filter(not(author.eq(any(ignore_bots))));
    }

    if let Some(c) = opts.channel {
        query = query.filter(channel.eq(c));
    }

    query
}

fn format_top(top: Vec<(String, i64)>) -> Vec<String> {
    let author_len = top.iter().map(|v| v.0.len()).max().unwrap_or(0);

    top.into_iter()
        .enumerate()
        .map(|(i, (author, count))| {
            format!(
                "{}. \x02{:<width$}\x0F {}",
                i + 1,
                author,
                count,
                width = author_len
            )
        })
        .collect()
}
