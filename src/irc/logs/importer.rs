use std::str::FromStr;
use std::time::Instant;

use actix::prelude::*;
use chrono::{LocalResult, NaiveDate, NaiveTime, TimeZone, Utc};
use diesel::prelude::*;
use futures::{future, Future, Stream};
use regex::Regex;
use reqwest::r#async::Client;

use crate::db::models::NewMessageAt;
use crate::db::{self, DbExecutor};
use crate::irc::{Irc, IrcMessage, IrcPlugin};
use crate::util::CommandParser;

#[derive(Debug, StructOpt)]
#[structopt(name = "import-logs", about = "Import from logs.fomalhaut.me")]
struct Opts {
    #[structopt(parse(try_from_str))]
    start_date: NaiveDate,

    #[structopt(parse(try_from_str))]
    end_date: Option<NaiveDate>,
}

pub struct ImportLogs {
    db: Addr<DbExecutor>,
    irc: Addr<Irc>,
    parser: CommandParser<Opts>,
}

impl Actor for ImportLogs {
    type Context = Context<Self>;
}

impl Handler<IrcMessage> for ImportLogs {
    type Result = ();

    fn handle(&mut self, msg: IrcMessage, ctx: &mut Self::Context) {
        lazy_static! {
            static ref MESSAGE_RE: Regex =
                Regex::new(r"^\[(\d{2}:\d{2}:\d{2})\] <([^>]+)> (.+)").unwrap();
        }

        let cmd = match self.parser.parse(&self.irc, msg) {
            Some(v) => v,
            None => return,
        };

        let mut futures = vec![];
        let mut date = cmd.opts.start_date;

        let last = Utc::today().naive_utc();
        let last = match cmd.opts.end_date {
            Some(v) if v <= last => v,
            _ => last,
        };

        let client = Client::new();

        loop {
            if date > last {
                break;
            }

            let db = self.db.clone();
            let fut = client
                .get(&format!("https://logs.fomalhaut.me/download/{}.log", date))
                .send()
                .and_then(|res| res.into_body().concat2())
                .from_err()
                .and_then(move |bytes| {
                    let s = String::from_utf8_lossy(&bytes).into_owned();

                    db::execute(&db, move |conn| {
                        use crate::db::schema::messages;

                        let mut messages = vec![];
                        let mut line_number = 1;

                        for line in s.lines() {
                            let c = match MESSAGE_RE.captures(line) {
                                Some(v) => v,
                                None => continue,
                            };

                            let time = c.get(1).unwrap().as_str();
                            let time = NaiveTime::from_str(time).unwrap();
                            let dt = chrono_tz::Europe::Kaliningrad
                                .from_local_datetime(&date.and_time(time));

                            let dt = match dt {
                                LocalResult::Single(v) => v.naive_utc(),
                                _ => {
                                    log::warn!("Invalid message time, skipping: {} {}", date, time);
                                    continue;
                                }
                            };

                            messages.push(NewMessageAt {
                                author: c.get(2).unwrap().as_str(),
                                channel: "#cc.ru",
                                body: c.get(3).unwrap().as_str(),
                                line_number,
                                sent_at: dt,
                                date_sent: date,
                            });

                            line_number += 1;
                        }

                        conn.transaction(|| {
                            diesel::delete(messages::table
                                .filter(
                                    messages::columns::channel.eq("#cc.ru")
                                           .and(messages::columns::date_sent.eq(date))
                                ))
                                .execute(&conn)?;

                            diesel::insert_into(messages::table)
                                .values(&messages)
                                .execute(&conn)
                        })?;

                        Ok(())
                    })
                });

            futures.push(fut);
            date = date.succ();
        }

        let t = Instant::now();

        ctx.wait(
            future::join_all(futures)
                .map(|_| ())
                .map_err(|e| {
                    log::error!("Failed to import logs: {}", e);
                })
                .into_actor(self)
                .map(move |_, actor, _| {
                    actor.irc.do_send(IrcMessage::privmsg(
                        cmd.channel,
                        format!("logs imported in {:?}", t.elapsed()),
                    ));
                }),
        );
    }
}

inventory::submit! {
    IrcPlugin::new(|c| {
        ImportLogs {
            db: c.pool.clone(),
            irc: c.irc.clone(),
            parser: CommandParser::new(&c.config)
        }.start().recipient()
    })
}
