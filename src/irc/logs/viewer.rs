use actix::prelude::*;
use chrono::NaiveDate;
use diesel::prelude::*;

use crate::db::{self, DbExecutor};
use crate::db::models::Message;
use crate::irc::{Irc, IrcMessage, IrcPlugin};
use crate::util::CommandParser;

#[derive(Debug, StructOpt)]
#[structopt(name = "log", about = "View logs")]
struct LogOpts {
    /// Channel name
    pub channel: String,

    /// Date
    pub date: NaiveDate,

    /// Log line number
    pub num: i32,
}

pub struct Log {
    db: Addr<DbExecutor>,
    irc: Addr<Irc>,
    parser: CommandParser<LogOpts>,
}

impl Actor for Log {
    type Context = Context<Self>;
}

impl Handler<IrcMessage> for Log {
    type Result = ();

    fn handle(&mut self, msg: IrcMessage, ctx: &mut Self::Context) {
        let cmd = match self.parser.parse(&self.irc, msg) {
            Some(v) => v,
            None => return,
        };

        if cmd.opts.num < 1 {
            return;
        }

        let channel = cmd.channel.clone();
        let channel2 = channel.clone();

        ctx.wait(
            db::execute(&self.db, move |conn| {
                use db::schema::messages::dsl::*;

                Ok(messages
                    .filter(
                        channel
                            .eq(cmd.opts.channel)
                            .and(date_sent.gt(cmd.opts.date)
                                          .or(date_sent.eq(cmd.opts.date)
                                                       .and(line_number.ge(cmd.opts.num))))
                    )
                    .order((date_sent.asc(), line_number.asc()))
                    .limit(5)
                    .load::<Message>(&conn)?)
            })
            .into_actor(self)
            .map_err(move |e, actor, _| {
                log::error!("Failed to show logs: {}", e);
                actor.irc.do_send(IrcMessage::privmsg(channel, format!("an error occured: {}", e)));
            })
            .map(move |logs, actor, _| {
                for line in format_log(logs) {
                    actor.irc.do_send(IrcMessage::privmsg(channel2.clone(), line));
                }
            })
        );
    }
}

inventory::submit! {
    IrcPlugin::new(|c| {
        Log {
            db: c.pool.clone(),
            irc: c.irc.clone(),
            parser: CommandParser::new(&c.config),
        }.start().recipient()
    })
}

pub fn format_log(log: Vec<Message>) -> Vec<String> {
    if log.is_empty() {
        return vec!["\x0304no data".into()];
    }

    // :log #cc.ru 500 2019-04-05 │ 10:33:38 fingercomp │ 2. войс Владимиру
    // :log #cc.ru 500 2019-04-05 │ 10:33:38   LeshaInc │ ???????????????????????????????????
    //                                                  │ ?????

    let author_len = log.iter().map(|v| v.author.len()).max().unwrap_or(0);
    let line_len = (log.iter().map(|v| v.line_number).max().unwrap_or(0) as f32)
        .log10()
        .ceil() as usize;
    let indent = format!("{}\x0314│\x0f ", " ".repeat(38 + author_len));

    let mut lines = vec![];

    for Message { channel, author, body, sent_at, line_number, .. } in log {
        let mut wrap = textwrap::wrap_iter(&body, 80);
        lines.push(format!(
            ":log {channel} {line:>line_width$} {date} \x0314│ \x0315{time} \x0f{author:>author_width$} \x0314│\x0f {message}",
            channel = channel,
            line = line_number,
            date = sent_at.format("%Y-%m-%d"),
            time = sent_at.format("%H:%M:%S"),
            author = author,
            message = wrap.next().unwrap(),
            author_width = author_len,
            line_width = line_len,
        ));

        for part in wrap {
            lines.push(format!("{}{}", indent, part));
        }
    }

    lines
}
