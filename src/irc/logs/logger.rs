use actix::prelude::*;
use chrono::{NaiveDate, Utc};
use diesel::prelude::*;
use diesel::sql_types::{Int4, Text};
use futures::Future;
use ::irc::proto::{Command, Prefix};

use crate::db::{self, DbExecutor};
use crate::irc::{IrcMessage, IrcPlugin};

diesel::sql_function!(fn next_line_number(chan: Text) -> Int4);

pub struct LogCollector {
    db: Addr<DbExecutor>,
}

impl Actor for LogCollector {
    type Context = Context<Self>;
}

impl Handler<IrcMessage> for LogCollector {
    type Result = ();

    fn handle(&mut self, msg: IrcMessage, ctx: &mut Self::Context) {
        let IrcMessage { prefix, command } = msg;

        let author = match prefix {
            Some(Prefix::Nickname(n, _, _)) => n,
            _ => return,
        };

        let (channel, body) = match command {
            Command::PRIVMSG(target, body) => (target, body),
            _ => return,
        };

        if !channel.starts_with("#") {
            return;
        }

        ctx.spawn(
            db::execute(&self.db, move |conn| {
                use crate::db::schema::messages;

                let new_msg = (
                    messages::columns::author.eq(&author),
                    messages::columns::channel.eq(&channel),
                    messages::columns::body.eq(&body),
                    messages::columns::line_number.eq(next_line_number(&channel))
                );

                diesel::insert_into(messages::table)
                    .values(&new_msg)
                    .execute(&conn)?;

                Ok(())
            })
            .map_err(|e| {
                log::error!("Failed to add log entry: {}", e);
            })
            .into_actor(self),
        );
    }
}

inventory::submit!(IrcPlugin::new(|c| LogCollector { db: c.pool.clone() }
    .start()
    .recipient()));
