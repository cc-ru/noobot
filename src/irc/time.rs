use actix::prelude::*;
use chrono::Utc;
use diesel::prelude::*;

use crate::db::models::{NewUserTimezone, UserTimezone};
use crate::db::{self, DbExecutor};
use crate::irc::{Irc, IrcMessage, IrcPlugin};
use crate::util::CommandParser;

#[derive(Debug, StructOpt)]
#[structopt(name = "time")]
struct TimeOpts {
    /// Nickname or timezone
    zone: Option<String>,
}

pub struct Time {
    db: Addr<DbExecutor>,
    irc: Addr<Irc>,
    parser: CommandParser<TimeOpts>,
}

impl Actor for Time {
    type Context = Context<Self>;
}

impl Handler<IrcMessage> for Time {
    type Result = ();

    fn handle(&mut self, msg: IrcMessage, ctx: &mut Self::Context) {
        let cmd = match self.parser.parse(&self.irc, msg) {
            Some(v) => v,
            None => return,
        };

        let tz = cmd.opts.zone.clone();
        let author = cmd.author.clone();

        let now = Utc::now();
        let tz = db::execute(&self.db, move |conn| {
            use crate::db::schema::user_timezones::dsl::*;

            if let Some(tz) = tz.as_ref().and_then(|v| v.parse::<chrono_tz::Tz>().ok()) {
                return Ok(Some(now.with_timezone(&tz).to_rfc2822()));
            }

            let nick = tz.unwrap_or(author);
            let tz = user_timezones
                .find(&nick)
                .first::<UserTimezone>(&conn)
                .optional()?
                .and_then(|v| v.timezone.parse::<chrono_tz::Tz>().ok());

            Ok(tz.map(|v| now.with_timezone(&v).to_rfc2822()))
        });

        ctx.wait(
            tz.into_actor(self)
                .map_err(move |e, _, _| {
                    log::error!("Failed to get user timezone: {}", e);
                })
                .map(move |time, actor, _| {
                    actor.irc.do_send(IrcMessage::privmsg(
                        cmd.channel,
                        if let Some(time) = time {
                            format!("{}: {}", cmd.author, time)
                        } else {
                            format!("{}: I lost my watch", cmd.author)
                        },
                    ))
                }),
        );
    }
}

inventory::submit! {
    IrcPlugin::new(|c| {
        Time {
            db: c.pool.clone(),
            irc: c.irc.clone(),
            parser: CommandParser::new(&c.config)
        }.start().recipient()
    })
}

#[derive(Debug, StructOpt)]
#[structopt(name = "settz")]
struct SetTzOpts {
    /// String
    timezone: String,
}

pub struct SetTz {
    db: Addr<DbExecutor>,
    irc: Addr<Irc>,
    parser: CommandParser<SetTzOpts>,
}

impl Actor for SetTz {
    type Context = Context<Self>;
}

impl Handler<IrcMessage> for SetTz {
    type Result = ();

    fn handle(&mut self, msg: IrcMessage, ctx: &mut Self::Context) {
        let cmd = match self.parser.parse(&self.irc, msg) {
            Some(v) => v,
            None => return,
        };

        if cmd.opts.timezone.parse::<chrono_tz::Tz>().is_err() {
            self.irc.do_send(IrcMessage::privmsg(
                cmd.channel,
                format!("{}: Invalid timezone", cmd.author),
            ));
            return;
        }

        let tz = cmd.opts.timezone.clone();
        let author = cmd.author.clone();

        let insert = db::execute(&self.db, move |conn| {
            use crate::db::schema::user_timezones::dsl::*;

            let user_tz = NewUserTimezone {
                nickname: &author,
                timezone: &tz,
            };

            diesel::insert_into(user_timezones)
                .values(&user_tz)
                .on_conflict(nickname)
                .do_update()
                .set(&user_tz)
                .execute(&conn)?;

            Ok(())
        });

        ctx.wait(
            insert
                .into_actor(self)
                .map_err(move |e, _, _| {
                    log::error!("Failed to update user timezone: {}", e);
                })
                .map(move |_, actor, _| {
                    actor.irc.do_send(IrcMessage::privmsg(
                        cmd.channel,
                        format!("{}: Roger that", cmd.author),
                    ));
                }),
        );
    }
}

inventory::submit! {
    IrcPlugin::new(|c| {
        SetTz {
            db: c.pool.clone(),
            irc: c.irc.clone(),
            parser: CommandParser::new(&c.config)
        }.start().recipient()
    })
}
