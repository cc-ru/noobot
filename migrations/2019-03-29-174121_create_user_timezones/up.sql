CREATE TABLE user_timezones (
    nickname    TEXT NOT NULL PRIMARY KEY,
    timezone    TEXT NOT NULL
);
