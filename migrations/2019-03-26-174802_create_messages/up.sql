CREATE TABLE messages (
    id      SERIAL PRIMARY KEY,
    channel TEXT NOT NULL,
    author  TEXT NOT NULL,
    body    TEXT NOT NULL,
    sent_at TIMESTAMP NOT NULL DEFAULT now()
);
