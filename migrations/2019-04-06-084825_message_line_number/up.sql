ALTER TABLE messages
  ADD COLUMN line_number integer,
  ADD COLUMN date_sent date DEFAULT current_date;

CREATE INDEX messages_sent_at_idx ON messages (
  sent_at datetime_ops ASC
);

CREATE INDEX messages_log_sort_idx ON messages (
  channel ASC,
  date_sent date_ops ASC,
  line_number ASC
);

CREATE INDEX temp_enumeration_idx ON messages (
  channel ASC,
  ( sent_at::date ) date_ops ASC,
  sent_at datetime_ops ASC
);

UPDATE messages
  SET line_number = n,
      date_sent = sent_at::date
  FROM (
    SELECT id,
           channel,
           row_number() OVER (PARTITION BY channel, sent_at::date ORDER BY sent_at)
                        AS n
      FROM messages
  ) AS enumerated
  WHERE messages.id = enumerated.id AND messages.channel = enumerated.channel;

DROP INDEX temp_enumeration_idx;

ALTER TABLE messages
  ALTER COLUMN line_number
    SET NOT NULL,
  ALTER COLUMN date_sent
    SET NOT NULL;

CREATE OR REPLACE FUNCTION next_line_number(chan text, OUT line_number integer) AS $$
BEGIN
  SELECT count(*) + 1
    INTO line_number
    FROM messages
    WHERE date_sent = current_date AND messages.channel = chan;
END
$$ LANGUAGE plpgsql;
