ALTER TABLE messages
  DROP COLUMN IF EXISTS line_number,
  DROP COLUMN IF EXISTS date_sent;

DROP INDEX IF EXISTS messages_sent_at_idx;
DROP INDEX IF EXISTS messages_log_sort_idx;
DROP FUNCTION IF EXISTS next_line_number;
